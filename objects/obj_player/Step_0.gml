if (Game.fsm.current_state_name == "Paused") exit;

#region Input Checks

	var _to_move_x = 0;
	var _to_move_y = 0;

	if (input_check("walk_right"))	_to_move_x = self.walk_speed;
	if (input_check("walk_left"))	_to_move_x = -self.walk_speed;
	if (input_check("walk_down"))	_to_move_y = self.walk_speed;
	if (input_check("walk_up"))		_to_move_y = -self.walk_speed;
		
#endregion

#region Collisions

	var _tilemap = layer_tilemap_get_id(layer_get_id("lyr_Pathfinding"));
	var _sgn_x = sign(_to_move_x);
	var _sgn_y = sign(_to_move_y);

	if (_sgn_x != 0) {
		var _start_x = _sgn_x == 1 ? bbox_right : bbox_left;
		if (place_meeting(_start_x+_to_move_x, y, cls_Collisionable)) {	
			while (!place_meeting(_start_x+_sgn_x, y, cls_Collisionable)) {
				x += _sgn_x;
				_start_x = _sgn_x == 1 ? bbox_right : bbox_left;
			}
		}
		else if (tilemap_get_at_pixel(_tilemap, _start_x+_to_move_x, y) == 1) {
			while (tilemap_get_at_pixel(_tilemap, _start_x+_sgn_x, y) != 1) {
				x += _sgn_x;
				_start_x = _sgn_x == 1 ? bbox_right : bbox_left;
			}
		}
		else {
			x += _to_move_x;
		}
	}
	if (_sgn_y != 0) {
		var _start_y = _sgn_y == 1 ? bbox_bottom : bbox_top;
		if (place_meeting(x, _start_y+_to_move_y, cls_Collisionable)) {	
			while (!place_meeting(x, _start_y+_sgn_y, cls_Collisionable)) {
				 y += _sgn_y;
				 _start_y = _sgn_y == 1 ? bbox_bottom : bbox_top;
			}
		}
		else if (tilemap_get_at_pixel(_tilemap, x, _start_y+_to_move_y) == 1) {
			while (tilemap_get_at_pixel(_tilemap, x, _start_y+_sgn_y) != 1) {
				y += _sgn_y;
				_start_y = _sgn_y == 1 ? bbox_bottom : bbox_top;
			}
		}
		else {
			y += _to_move_y;
		}
	}

#endregion

#region Pathfinding
	
	if (input_check_pressed("move")) {
		self.move_target = {x: device_mouse_x(0), y: device_mouse_y(0)};
		self.move_path = path_add();
		var _path_exists = mp_grid_path(Game.pathfinding_grid, self.move_path, self.x, self.y, self.move_target.x, self.move_target.y, false);	
		if (_path_exists) {
			path_start(self.move_path, self.move_speed, path_action_stop, false);
		}		
	}
	
	// Cancel path if reached the end or a manual movement key has been pressed
	if (path_exists(self.move_path) && (path_position == 1 || input_check(["walk_right","walk_left","walk_down","walk_up"]))) {
		self.move_target = {x: -1, y: -1};
		if (path_exists(self.move_path))	path_delete(self.move_path);
	}
	
#endregion


x = round(x);
y = round(y);
