self.debug = false;

#region Game State

	self.fsm = new StateMachine("Game State");
	with (self.fsm) {
		add(new State("Menu"));
		add(new State("In Game"));
		add(new State("Paused"));
	
		init("Main Menu");
	}

#endregion

room_goto(FIRST_ROOM);

#region Debug

	show_debug_overlay(self.debug);

#endregion
