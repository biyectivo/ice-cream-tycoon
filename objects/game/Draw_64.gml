#region Debug screen data
	if (self.debug) {
		backup_draw_settings();
		draw_debug(true);
		restore_draw_settings();
	}
#endregion