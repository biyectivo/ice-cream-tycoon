randomize();
//random_set_seed(3490961328);
#region Population

	var _t = get_timer();
	create_population();
	_t = (get_timer()-_t)/1000000;
	show_debug_message("Time: "+string(_t));

#endregion

#region Pathfinding

	if (room == room_Test) {
		self.pathfinding_grid = mp_grid_create(0, 0, ceil(room_width div PATHFINDING_CELL_SIZE), ceil(room_height div PATHFINDING_CELL_SIZE), PATHFINDING_CELL_SIZE, PATHFINDING_CELL_SIZE);
		var _tilemap = layer_tilemap_get_id(layer_get_id("lyr_Pathfinding"));
		// Add collisionable tiles
		for (var _col=0; _col<room_width div TILE_SIZE; _col++) {
			for (var _row=0; _row<room_height div TILE_SIZE; _row++) {
				if (tilemap_get(_tilemap, _col, _row) == 1) {
					var _multiplier = TILE_SIZE div PATHFINDING_CELL_SIZE;
					for (var _i=0; _i<_multiplier; _i++) {
						for (var _j=0; _j<_multiplier; _j++) {
							mp_grid_add_cell(self.pathfinding_grid, _multiplier * _col + _i, _multiplier * _row + _j);
						}
					}					
				}
			}
		}
		// Add collisionable objects
		mp_grid_add_instances(Game.pathfinding_grid, cls_Collisionable, true);
	}
	
#endregion