function Person(_name_set, _name_country, _first_name, _middle_initial, _last_name, _birth_year, _birth_month, _birth_day, _occupation, _height_cm, _weight_kg, _gender) constructor {
	// Controllable
	self.controllable = false;
	// Fake name generator specific data
	self.name_set = _name_set;
	self.name_country = _name_country;
	// Demographic data
	self.first_name = _first_name;
	self.middle_initial = _middle_initial;
	self.last_name = _last_name;
	self.birth_year = _birth_year;
	self.birth_month = _birth_month;
	self.birth_day = _birth_day;
	self.occupation = _occupation;
	self.height_cm = _height_cm;
	self.weight_kg = _weight_kg;
	self.gender = _gender;
	// Taste profile
	
	// Order history
	
	// Inventory
	
	// Schedule
	
	// Game location
	self.room = room_Test;
	self.x = 0;
	self.y = 0;
	self.state = -1;
	
	// Methods
	static getName = function()	{
		return self.first_name+" "+self.last_name;
	}
	static getHeight = function(_imperial=false) {
		if (_imperial) {
			var _feet = self.height_cm/30.48;
			var _inches = round(frac(_feet)*12);
			return string(floor(_feet))+" ft "+string(_inches)+" in";
		}
		else return string(self.height_cm)+" cm";
	}
	static getWeight = function(_imperial=false) {
		return _imperial ?	string(round(self.weight_kg * 2.204623))+" lb" :
							string(self.weight_kg)+" kg";
	}
	static getAge = function(_y = current_year, _m = current_month, _d = current_day) {
		return (_m < self.birth_month || (_m == self.birth_month && _d < self.birth_day)) ? _y - self.birth_year - 1 : _y - self.birth_year;
	}
	static isBirthday = function(_m = current_month, _d = current_day) {
		return (_m == self.birth_month && _d == self.birth_day);
	}
}

///@function				load_person_data(_field_name_array, _locale)
///@description				loads specific fields from fake person data generated from fakenamegenerator.com
///@param		{array}		_field_name_array	The array with field names to load
///@param		{string}	_locale				The locale file to use - country and nameset
///@param		{string}	_path				The path of the files
///@param		{string}	_file_prefix		The prefix of the file to use
///@param		{string}	_file_suffix		The suffix to use before the extension
function load_person_data(_field_name_array, _locale = "en_us", _path="C:\\Users\\biyec\\OneDrive\\Documentos\\GameMakerStudio2\\games\\Ice Cream Tycoon\\database", _file_prefix="person_data", _file_suffix="") {
	try {
		var _fid = file_text_open_read(_path+"\\"+_file_prefix+"_"+_locale+"_"+_file_suffix+".csv");
		var _line = file_text_readln(_fid);
		var _vars = string_split(_line, ",");		
		var _pos = [];
		for (var _i=0, _n=array_length(_field_name_array); _i<_n; _i++) {
			array_push(_pos, array_find(_vars, _field_name_array[_i]));
		}
		
		var _data = [];
		var _rownum = 0;
		while (!file_text_eof(_fid)) {			
			_line = file_text_readln(_fid);			
			if (string_length(_line)>2) {
				_rownum += 1;
				_vars = string_split(_line, ",");			
				//show_debug_message(string(_rownum)+": ("+string(array_length(_vars))+") "+_line);
				var _row = [];
				for (var _i=0, _n=array_length(_pos); _i<_n; _i++) {				
					array_push(_row, _vars[_pos[_i]]);
				}
				array_push(_data, _row);
			}
		}
		return _data;
		
		file_text_close(_fid);
	}
	catch (_exception) {
		show_debug_message(_exception.longMessage);
	}	
}

///@function			create_population
///@description			creates the game population
function create_population() {
	var _chunk = irandom_range(0, POPULATION_FILE_CHUNKS-1);
	//var _chunk = 5;
	show_debug_message("Chunk: "+string(_chunk));
	show_debug_message("Seed: "+string(random_get_seed()));
	Game.person_pool = load_person_data(["NameSet","Country","GivenName","MiddleInitial","Surname","Birthday","Occupation","Centimeters","Kilograms","Gender"],,"database","person_data",string(_chunk));
	var _n = irandom_range(STARTING_POPULATION_MIN, STARTING_POPULATION_MAX);
	var _idx = array_create_numeric(0, array_length(Game.person_pool)-1);
	Game.population_indices = random_sample(_idx, _n, true);
	Game.population = [];
	//show_debug_message("Sample indices: "+string(Game.population_indices));
	for (var _i=0; _i<_n; _i++) {
		//show_debug_message(" Person "+string(_i)+": "+string(Game.person_pool[Game.population_indices[_i]]));
		var _date_split = string_split(Game.person_pool[Game.population_indices[_i]][5], "/");
		array_push(Game.population, new Person(	Game.person_pool[Game.population_indices[_i]][0],
												Game.person_pool[Game.population_indices[_i]][1],
												Game.person_pool[Game.population_indices[_i]][2],
												Game.person_pool[Game.population_indices[_i]][3],
												Game.person_pool[Game.population_indices[_i]][4],
												real(_date_split[2]),
												real(_date_split[0]),
												real(_date_split[1]),
												Game.person_pool[Game.population_indices[_i]][6],
												real(Game.person_pool[Game.population_indices[_i]][7]),
												Game.person_pool[Game.population_indices[_i]][8],
												Game.person_pool[Game.population_indices[_i]][9]));
		
	}
}



///@function			draw_debug
///@description			Shows debug data about display, view, GUI, etc.
///@param		{bool}	_to_screen	If true draws to GUI - needs to be called in Draw GUI - if false outputs to log

function draw_debug(_to_screen = false) {
	var _data = [];
	array_push(_data, "Display: "+string(DISPLAY_W)+"x"+string(DISPLAY_H));
	array_push(_data, "Display aspect ratio: "+string(Camera.display_aspect_ratio));	
	array_push(_data, "Fullscreen: "+string(window_get_fullscreen()));
	array_push(_data, "Window: "+string(WINDOW_W)+"x"+string(WINDOW_H));
	array_push(_data, "Window scaling: "+string(Camera.window_scale));
	array_push(_data, "View coords: "+string(CAM_X)+","+string(CAM_Y));
	array_push(_data, "View size (perfect scaling): "+string(CAM_W)+"x"+string(CAM_H));
	array_push(_data, "Selected aspect ratio for perfect scaling: "+string(Camera.aspect_ratio));
	array_push(_data, "Ideal view size: "+string(Camera.ideal_width)+"x"+string(Camera.ideal_height));
	array_push(_data, "Application surface: "+string(APP_SURFACE_W)+"x"+string(APP_SURFACE_H));
	array_push(_data, "GUI size: "+string(GUI_W)+"x"+string(GUI_H));
	array_push(_data, "GUI scaling: "+string(Camera.gui_scale));
	array_push(_data, "Room size: "+string(room_get_name(room))+" "+string(room_width)+"x"+string(room_height));
	array_push(_data, "View enabled: "+string(view_enabled)+" View visible: "+string(view_visible[0]));
	array_push(_data, "Player x,y: "+(instance_exists(obj_Player) ? string(obj_Player.x)+","+string(obj_Player.y) : "Not present"));
	array_push(_data, "Random seed: "+string(random_get_seed()));
	array_push(_data, "Seconds since start: "+string(get_timer()/1000000));
	
	var _height=0;
	var _spacing=10;
	if (_to_screen) {	
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		draw_set_font(fnt_Debug);
		_height = string_height("A");
		draw_set_color(c_black);
		draw_set_alpha(0.5);
		draw_rectangle(0, 0, GUI_W, array_length(_data) * (_height+_spacing), false);
		draw_set_alpha(1);
		draw_set_color(c_white);
	}	
	
	for (var _i=0, _n=array_length(_data); _i<_n; _i++) {
		if (_to_screen)		draw_text(10, 10 + _i*(_height+_spacing), _data[_i]);
		else				show_debug_message(_data[_i]);		
	}
		
}
