INPUT_DEFAULT_PROFILES = {   
    
	keyboard_and_mouse: {
		walk_up:		[input_binding_key(vk_up), input_binding_key(ord("W"))],
		walk_down:		[input_binding_key(vk_down), input_binding_key(ord("S"))],
		walk_left:		[input_binding_key(vk_left), input_binding_key(ord("A"))],
		walk_right:		[input_binding_key(vk_right), input_binding_key(ord("D"))],		
		move:			input_binding_mouse_button(mb_left),		
		interact:		input_binding_mouse_button(mb_left),
		examine:		input_binding_mouse_button(mb_right),
		pause:			input_binding_key(vk_escape)
	},
	
	gamepad: {
		walk_up:		[input_binding_gamepad_axis(gp_axislv, true),  input_binding_gamepad_button(gp_padu)],
        walk_down:		[input_binding_gamepad_axis(gp_axislv, false), input_binding_gamepad_button(gp_padd)],
        walk_left:		[input_binding_gamepad_axis(gp_axislh, true),  input_binding_gamepad_button(gp_padl)],
        walk_right:		[input_binding_gamepad_axis(gp_axislh, false), input_binding_gamepad_button(gp_padr)],
		move:			input_binding_empty(),
		interact:		input_binding_gamepad_button(gp_face1),
		examine:		input_binding_gamepad_button(gp_face2),
		pause:			input_binding_gamepad_button(gp_start)		
	}
    
};

#macro INPUT_AUTO_PROFILE_FOR_KEYBOARD				"keyboard_and_mouse"
#macro INPUT_AUTO_PROFILE_FOR_MOUSE					"keyboard_and_mouse"
#macro INPUT_AUTO_PROFILE_FOR_GAMEPAD				"gamepad"
#macro INPUT_AUTO_PROFILE_FOR_MIXED					"mixed"
#macro INPUT_AUTO_PROFILE_FOR_MULTIDEVICE			"multidevice"
#macro INPUT_ASSIGN_KEYBOARD_AND_MOUSE_TOGETHER		true
#macro INPUT_ALLOW_ASSYMMETRIC_DEFAULT_PROFILES		true