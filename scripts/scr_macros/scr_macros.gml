#macro		GAME_NAME						"Ice Cream Tycoon"
											
#macro		FIRST_ROOM						room_Menu
#macro		Debug:FIRST_ROOM				room_Test
											
#macro		GUI_W							display_get_gui_width()
#macro		GUI_H							display_get_gui_height()
#macro		DISPLAY_W						display_get_width()
#macro		DISPLAY_H						display_get_height()
#macro		WINDOW_W						window_get_width()
#macro		WINDOW_H						window_get_height()
#macro		APP_SURFACE_W					surface_get_width(application_surface)
#macro		APP_SURFACE_H					surface_get_height(application_surface)

#macro		TILE_SIZE						16
#macro		PATHFINDING_CELL_SIZE			(TILE_SIZE/2)

#macro		STARTING_POPULATION_MIN			500
#macro		STARTING_POPULATION_MAX			1500
#macro		POPULATION_FILE_CHUNKS			10
